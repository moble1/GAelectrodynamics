%
% Copyright � 2018 Peeter Joot.  All Rights Reserved.
% Licenced as described in the file LICENSE under the root directory of this GIT repository.
%
%{
%%%\input{../latex/blogpost.tex}
%%%\renewcommand{\basename}{surfaceintegral}
%%%%\renewcommand{\dirname}{notes/phy1520/}
%%%\renewcommand{\dirname}{notes/ece1228-electromagnetic-theory/}
%%%%\newcommand{\dateintitle}{}
%%%%\newcommand{\keywords}{}
%%%
%%%\input{../latex/peeter_prologue_print2.tex}
%%%
%%%\usepackage{peeters_layout_exercise}
%%%\usepackage{peeters_braket}
%%%\usepackage{peeters_figures}
%%%\usepackage{siunitx}
%%%%\usepackage{mhchem} % \ce{}
%%%%\usepackage{macros_bm} % \bcM
%%%%\usepackage{macros_qed} % \qedmarker
%%%\usepackage{txfonts} % \ointclockwise
%%%
%%%\beginArtNoToc
%%%
%%%\generatetitle{Multivector surface integral.}
%\section{Surface integral.}
%\label{chap:surfaceintegral}

\index{area element}
\index{differential form}
A two parameter curve, and the corresponding differentials with respect to those parameters, is plotted in
\cref{fig:twoParameterDifferential:twoParameterDifferentialFig1}.

\imageFigure{../figures/GAelectrodynamics/twoParameterDifferentialFig1}{Two parameter manifold differentials.}{fig:twoParameterDifferential:twoParameterDifferentialFig1}{0.4}

Given parameters \( a, b \), the differentials along each of the parameterization directions are
\begin{dmath}\label{eqn:surfaceintegral:100}
\begin{aligned}
d\Bx_a &= \PD{a}{\Bx} da = \Bx_a da \\
d\Bx_b &= \PD{b}{\Bx} db = \Bx_b db.
\end{aligned}
\end{dmath}

The bivector valued surface area element for this parameterization is
\begin{equation}\label{eqn:surfaceintegral:120}
d^2 \Bx
=
d\Bx_a \wedge
d\Bx_b
=
da db (\Bx_a \wedge \Bx_b).
\end{equation}

The vector derivative, the projection of the gradient onto the surface at the point of integration (also called the tangent space), now has two components
\begin{dmath}\label{eqn:surfaceintegral:200}
\boldpartial
=
\sum_i \Bx^i (\Bx_i \cdot \spacegrad)
=
\Bx^a \PD{a}{}
+
\Bx^b \PD{b}{}
\equiv
\Bx^a \partial_a
+
\Bx^b \partial_b.
\end{dmath}

The surface integral specialization of \cref{dfn:fundamentalTheoremOfCalculus:240} can now be stated

\makedefinition{Multivector surface integral.}{dfn:surfaceintegral:100}{
Given an connected surface \( S \) parameterized by two parameters, and multivector functions \( F, G \), we define the surface integral as
\begin{equation*}
\int_S F d^2\Bx \boldpartial G
\equiv
\int_S \lr{ F d^2\Bx \lboldpartial} G
+
\int_S F d^2\Bx \lr{ \rboldpartial G },
\end{equation*}
where the two parameter differential form \( d^2 \Bx = da db\, \Bx_a \wedge \Bx_b \) varies over the surface.
} % definition

%%As mentioned in a line integral context,
%%multivectors may not commute with the vector derivative or the differential, so we allow the vector derivative to act bidirectionally using the chain rule.
%%The scope of the action of the vector derivative when acting only to the left or right is indicated using braces above.
%%Should we wish to only integrate single functions, we can set either of the other to \( 1 \), yielding integrals of the form
%%\( \int_S F d^2\Bx \lboldpartial, \) or \( \int_S d^2\Bx \boldpartial G \).

The surface integral specialization of \cref{thm:fundamentalTheoremOfCalculus:1} is

\input{Theorem_multivector_surface_integral.tex}
In \R{2} with \( d^2 \Bx = i dA, i = \Be_{12} \), this may be written
\begin{dmath}\label{eqn:surfaceintegral:760}
\int_S F i \spacegrad G\, dA
= \ointclockwise_{\partial S} F d\Bx G.
\end{dmath}
As \( i \) does not commute with all \R{2} multivectors, unless \( F = 1 \) we cannot generally pull \( i \) out of the integral.
In \R{3}, with \( d^2 \Bx = I \ncap dA \), we may use a scalar area element, but cannot generally replace the vector derivative with the gradient.
%\begin{dmath}\label{eqn:surfaceintegral:780}
%I \int_S F \ncap \boldpartial G\, dA
%= \ointclockwise_{\partial S} F d\Bx G.
%\end{dmath}

To see why this works, we would first like to reduce the product of the area element and the vector derivative
\begin{dmath}\label{eqn:surfaceintegral:300}
d^2\Bx \boldpartial
=
da db\, \lr{ \Bx_a \wedge \Bx_b } \lr{ \Bx^a \partial_a + \Bx^b \partial_b }.
\end{dmath}

Since \( \Bx^a \in \Span \setlr{ \Bx_a, \Bx_b } \), this multivector product has only a vector grade.  That is
\begin{dmath}\label{eqn:surfaceintegral:320}
\lr{ \Bx_a \wedge \Bx_b } \Bx^a
=
\lr{ \Bx_a \wedge \Bx_b } \cdot \Bx^a
+
\cancel{ \lr{ \Bx_a \wedge \Bx_b } \wedge \Bx^a }
=
\lr{ \Bx_a \wedge \Bx_b } \cdot \Bx^a
=
\Bx_a \lr{ \Bx_b \cdot \Bx^a } -
\Bx_b \lr{ \Bx_a \cdot \Bx^a }
=
-\Bx_b.
\end{dmath}

Similarly
\begin{dmath}\label{eqn:surfaceintegral:340}
\lr{ \Bx_a \wedge \Bx_b } \Bx^b
=
\lr{ \Bx_a \wedge \Bx_b } \cdot \Bx^b
+
\cancel{ \lr{ \Bx_a \wedge \Bx_b } \wedge \Bx^b }
=
\lr{ \Bx_a \wedge \Bx_b } \cdot \Bx^b
=
\Bx_a \lr{ \Bx_b \cdot \Bx^b } -
\Bx_b \lr{ \Bx_a \cdot \Bx^b }
=
\Bx_a,
\end{dmath}
so
\begin{dmath}\label{eqn:surfaceintegral:360}
d^2\Bx \boldpartial
=
\Bx_a \partial_b
-\Bx_b \partial_a.
\end{dmath}

Inserting this into the surface integral, we find
\begin{dmath}\label{eqn:surfaceintegral:380}
\int_S F d^2\Bx \boldpartial G
=
\int_S \lr{ F d^2\Bx \lboldpartial} G
+
\int_S F d^2\Bx \lr{ \rboldpartial G }
=
\int_S da db\, \lr{ \partial_b F \Bx_a - \partial_a F \Bx_b } G
+
\int_S da db\, F \lr{ \Bx_a \partial_b G - \Bx_b \partial_a G }
=
\int_S da db\, \lr{ \PD{b}{F} \PD{a}{\Bx} - \PD{a}{F} \PD{b}{\Bx} } G
+
\int_S da db\, F \lr{ \PD{a}{\Bx} \PD{b}{G} - \PD{b}{\Bx} \PD{a}{G} }
=
\int_S da db\, \PD{b}{} \lr{ F \PD{a}{\Bx} G } - \int_S da db\, \PD{a}{} \lr{ F \PD{b}{\Bx} G }
-
\int_S da db\, F \lr{ \PD{b}{} \PD{a}{\Bx} - \PD{a}{} \PD{b}{\Bx} } G
=
\int_S da db\, \PD{b}{} \lr{ F \PD{a}{\Bx} G } - \int_S da db\, \PD{a}{} \lr{ F \PD{b}{\Bx} G }.
\end{dmath}

This leaves two perfect differentials, which can both be integrated separately.  That gives
\begin{dmath}\label{eqn:surfaceintegral:400}
\int_S F d^2\Bx \boldpartial G
=
\int_{\Delta a} da\, \evalbar{\lr{ F \PD{a}{\Bx} G }}{\Delta b} - \int_{\Delta b} db\, \evalbar{\lr{ F \PD{b}{\Bx} G }}{\Delta a}
=
\int_{\Delta a} \evalbar{\lr{ F d\Bx_a G }}{\Delta b} - \int_{\Delta b} \evalbar{\lr{ F d\Bx_b G }}{\Delta a}.
\end{dmath}

Suppose we are integrating over the unit parameter volume space \( [a, b] \in [0,1] \otimes [0,1] \) as illustrated in
\cref{fig:twoParameterDifferentialBoundary:twoParameterDifferentialBoundaryFig2}.
\imageFigure{../figures/GAelectrodynamics/twoParameterDifferentialBoundaryFig2}{Contour for two parameter surface boundary.}{fig:twoParameterDifferentialBoundary:twoParameterDifferentialBoundaryFig2}{0.4}

Comparing to the figure we see that we've ended up with a clockwise line integral around the boundary of the surface.
For a given subset of the surface, the bivector area element can be chosen small enough that it lies in the tangent space
to the surface at the point of integration.
In that case, a larger bounding loop can be conceptualized as the sum of a number of smaller ones, as sketched
in \cref{fig:loopIntegralInfinitesimalSum:loopIntegralInfinitesimalSumFig2},
in which case the
contributions of the interior loop segments cancel out.

\imageFigure{../figures/gabook/loopIntegralInfinitesimalSumFig2}{Sum of infinitesimal loops.}{fig:loopIntegralInfinitesimalSum:loopIntegralInfinitesimalSumFig2}{0.35}

\subsection{Two parameter Stokes' theorem.}

Two special cases of \cref{thm:surfaceintegral:100} when scalar and vector functions are integrated over a surface.  For scalar functions we have

\input{Theorem_surface_integral_of_scalar_stokes.tex}

To show the first part, we can split the (multivector) surface integral into vector and trivector grades
\begin{dmath}\label{eqn:surfaceintegral:440}
\int_S d^2\Bx \boldpartial f
=
\int_S d^2\Bx \cdot \boldpartial f
+
\int_S d^2\Bx \wedge \boldpartial f.
\end{dmath}

Since \( \Bx^a, \Bx^b \) both lie in the span of \( \setlr{ \Bx_a, \Bx_b } \),
\( d^2\Bx \wedge \boldpartial = 0 \), killing the second integral in \cref{eqn:surfaceintegral:440}.
If the gradient is decomposed into its projection along the tangent
space (the vector derivative) and its perpendicular components, only the vector derivative components of the
gradient contribute to its dot product with the area element.  That is
\begin{dmath}\label{eqn:surfaceintegral:460}
d^2 \Bx \cdot \spacegrad
=
d^2 \Bx \cdot \lr{ \Bx^a \partial_a + \Bx^b \partial_b + \cdots }
=
d^2 \Bx \cdot \lr{ \Bx^a \partial_a + \Bx^b \partial_b }
=
d^2 \Bx \cdot \boldpartial.
\end{dmath}

This means that for a scalar function
\begin{dmath}\label{eqn:surfaceintegral:480}
\int_S d^2\Bx \boldpartial f
=
\int_S d^2\Bx \cdot \spacegrad f.
\end{dmath}

The second part of the theorem follows by grade selection, and application of a duality transformation for the area element
\begin{dmath}\label{eqn:surfaceintegral:500}
d^2 \Bx \cdot \spacegrad f
=
\gpgradeone{ d^2 \Bx \spacegrad f }
=
dA \gpgradeone{ I \ncap \spacegrad f }
=
dA \gpgradeone{ I \lr{ \ncap \cdot \spacegrad f + I \ncap \cross \spacegrad f} }
=
-dA \ncap \cross \spacegrad f.
\end{dmath}

back substitution of \cref{eqn:surfaceintegral:500} completes the proof of \cref{thm:surfaceintegral:420}.

For vector functions we have

\input{Theorem_surface_integral_of_vector_stokes.tex}

%%This follows by setting \( F = 1, G = \Bf \) in \cref{thm:surfaceintegral:100} and selecting the scalar grade.  In particular we may form the
%%scalar selection of \( d^2 \Bx \boldpartial \Bf \) in two different ways.  The first is
%%\begin{dmath}\label{eqn:surfaceintegral:520}
%%\gpgradezero{ d^2 \Bx \boldpartial \Bf }
%%=
%%\gpgradezero{ (d^2 \Bx \cdot \boldpartial + d^2 \Bx \wedge \boldpartial ) \Bf }.
%%\end{dmath}
%%
%%The \( d^2 \Bx \wedge \boldpartial \) product with \( \Bf \) has only bivector and quad-vector components (the latter is zero in \R{3}), so its scalar grade selection is zero, and we are left with
%%\begin{dmath}\label{eqn:surfaceintegral:540}
%%\gpgradezero{ d^2 \Bx \boldpartial \Bf }
%%=
%%(d^2 \Bx \cdot \boldpartial) \cdot \Bf
%%=
%%(d^2 \Bx \cdot \spacegrad) \cdot \Bf,
%%\end{dmath}
%%where we have used \cref{eqn:surfaceintegral:460} again.  This product can also be written as
%%\begin{dmath}\label{eqn:surfaceintegral:560}
%%(d^2 \Bx \cdot \spacegrad) \cdot \Bf
%%=
%%\gpgradezero{ (d^2 \Bx \cdot \spacegrad) \Bf }
%%=
%%\gpgradezero{ (d^2 \Bx \spacegrad - d^2 \Bx \wedge \spacegrad) \Bf }
%%=
%%\gpgradezero{ d^2 \Bx \spacegrad \Bf }
%%=
%%\gpgradezero{ d^2 \Bx \lr{ \cancel{ \spacegrad \cdot \Bf } + \spacegrad \wedge \Bf } }
%%=
%%d^2 \Bx \cdot \lr{ \spacegrad \wedge \Bf }.
%%\end{dmath}
%%
%%\begin{dmath}\label{eqn:surfaceintegral:580}
%%\ointclockwise_{\partial S} d\Bx \cdot \Bf
%%=
%%\gpgradezero{ \int_S d^2\Bx \boldpartial \Bf }
%%=
%%\int_S \lr{ d^2\Bx \cdot \spacegrad } \cdot \Bf
%%=
%%\int_S d^2\Bx \cdot \lr{ \spacegrad \wedge \Bf },
%%\end{dmath}
%%as claimed.  In particular in \R{3}, we have
%%\begin{dmath}\label{eqn:surfaceintegral:600}
%%d^2\Bx \cdot \lr{ \spacegrad \wedge \Bf }
%%=
%%dA \gpgradezero{ I \ncap I \lr{ \spacegrad \cross \Bf } }
%%=
%%-dA \ncap \cdot \lr{ \spacegrad \cross \Bf }.
%%\end{dmath}
%%
%%Substitution into \cref{eqn:surfaceintegral:580} proves the last part of \cref{thm:surfaceintegral:500}.
%%
\subsection{Green's theorem.}

\Cref{thm:surfaceintegral:500}, when stated in terms of coordinates, is another well known result.
\input{Theorem_greens.tex}

FIXME: Add an example (lots to pick from in any 3rd term calc text)

The first equality in \cref{thm:surfaceintegral:620} holds in \R{N} for vectors expressed in terms of an arbitrary curvilinear basis.
Only the (curvilinear) coordinates of the vector \( \Bf \) contribute to this integral, and only those that lie in the tangent space.
The reciprocal basis vectors \( \Bx^i \) are also nowhere to be seen.  This is because they are either obliterated in dot products with \( \Bx_j \), or cancel due to mixed partial equality.

To see how this occurs let's look at the
area integrand of \cref{thm:surfaceintegral:500}
\begin{dmath}\label{eqn:surfaceintegral:660}
d^2 \Bx \cdot \lr{ \spacegrad \wedge \Bf }
=
du_1 du_2\, \lr{ \Bx_1 \wedge \Bx_2 } \cdot \lr{ \sum_{ij} \lr{ \Bx^i \partial_i } \wedge \lr{ f_j \Bx^j } }
=
du_1 du_2\, \sum_{ij} \lr{ \lr{ \Bx_1 \wedge \Bx_2 } \cdot \Bx^i } \cdot \lr{ \partial_i (f_j \Bx^j) }
=
du_1 du_2\, \sum_{ij} \lr{ \lr{ \Bx_1 \wedge \Bx_2 } \cdot \Bx^i } \cdot \Bx^j \partial_i f_j
+
du_1 du_2\, \sum_{ij} f_j \lr{ \lr{ \Bx_1 \wedge \Bx_2 } \cdot \Bx^i } \cdot (\partial_i \Bx^j).
\end{dmath}

With a bit of trouble, we will see that the second integrand is zero.  On the other hand, the first integrand
simplifies
without too much trouble
\begin{dmath}\label{eqn:surfaceintegral:680}
\sum_{ij} \lr{ \lr{ \Bx_1 \wedge \Bx_2 } \cdot \Bx^i } \cdot \Bx^j \partial_i f_j
=
\sum_{ij} \lr{ \Bx_1 \delta_{2i} - \Bx_2 \delta_{1i} } \cdot \Bx^j \partial_i f_j
=
\sum_{j} \Bx_1 \cdot \Bx^j \partial_2 f_j -\Bx_2 \cdot \Bx^j \partial_1 f_j
=
\partial_2 f_1 - \partial_1 f_2.
\end{dmath}

For the second integrand, we have
\begin{dmath}\label{eqn:surfaceintegral:700}
\sum_{ij} f_j \lr{ \lr{ \Bx_1 \wedge \Bx_2 } \cdot \Bx^i } \cdot (\partial_i \Bx^j)
=
\sum_{j} f_j \sum_i \lr{ \Bx_1 \delta_{2i} - \Bx_2 \delta_{1i} } \cdot (\partial_i \Bx_j)
=
\sum_{j} f_j
\lr{
\Bx_1 \cdot (\partial_2 \Bx^j)
-
\Bx_2 \cdot (\partial_1 \Bx^j)
}
\end{dmath}

We can apply the chain rule (backwards) to the portion in brackets to find
\begin{dmath}\label{eqn:surfaceintegral:720}
\Bx_1 \cdot (\partial_2 \Bx^j)
-
\Bx_2 \cdot (\partial_1 \Bx^j)
=
\cancel{\partial_2 \lr{ \Bx_1 \cdot \Bx^j }}
-
(\partial_2 \Bx_1) \cdot \Bx^j
-
\cancel{\partial_1 \lr{ \Bx_2 \cdot \Bx^j }}
+
(\partial_1 \Bx_2) \cdot \Bx^j
=
\Bx_j \cdot \lr{ \partial_1 \Bx_2 - \partial_2 \Bx_1 }
=
\Bx_j \cdot \lr{ \PD{u_1}{} \PD{u_2}{\Bx} - \PD{u_2}{} \PD{u_1}{\Bx} }
= 0.
\end{dmath}

In this reduction the derivatives of \( \Bx_i \cdot \Bx^j = \delta_{ij} \) were killed since those are constants (either zero or one).  The final step relies on the fact that we assume our vector parameterization is well behaved enough that the mixed partials are zero.

Substituting these results into
\cref{thm:surfaceintegral:500} we find
\begin{dmath}\label{eqn:surfaceintegral:740}
\ointclockwise_{\partial S} d\Bx \cdot \Bf
=
\ointclockwise_{\partial S} \lr{ du_1 \Bx_1 + du_2 \Bx_2 } \cdot \lr{ \sum_i f_i \Bx^i }
=
\ointclockwise_{\partial S} du_1 f_1 + du_2 f_2
=
\int_S du_1 du_2\, \lr{ \partial_2 f_1 - \partial_1 f_2 },
\end{dmath}
which completes the proof.

%}
%%%\EndArticle
