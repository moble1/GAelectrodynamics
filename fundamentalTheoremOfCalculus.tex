%
% Copyright � 2016 Peeter Joot.  All Rights Reserved.
% Licenced as described in the file LICENSE under the root directory of this GIT repository.
%
%{
%\input{../blogpost.tex}
%\renewcommand{\basename}{fundamentalTheoremOfCalculus}
%\renewcommand{\dirname}{notes/phy1520/}
%%\newcommand{\dateintitle}{}
%%\newcommand{\keywords}{}
%
%\input{../peeter_prologue_print2.tex}
%
%\usepackage{peeters_layout_exercise}
%\usepackage{peeters_braket}
%\usepackage{peeters_figures}
%\usepackage{siunitx}
%
%\beginArtNoToc
%
%\generatetitle{Fundamental theorem of geometric calculus}
%\label{chap:fundamentalTheoremOfCalculus}

\subsection{Hypervolume integral}
We wish to generalize the concepts of line, surface and volume integrals to hypervolumes and multivector functions, and define a hypervolume integral as

\makedefinition{Multivector integral.}{dfn:fundamentalTheoremOfCalculus:240}{
Given a hypervolume parameterized by \( k \) parameters, k-volume volume element \( d^k \Bx \), and
multivector functions \( F, G \), a k-volume integral with the vector derivative acting to the right on \( G \) is written as
\begin{equation*}
\int_V d^k\Bx \rboldpartial G,
\end{equation*}
a k-volume integral with the vector derivative acting to the left on \( F \) is written as
\begin{equation*}
\int_V F d^k\Bx \lboldpartial,
\end{equation*}
and a k-volume integral with the vector derivative acting bidirectionally on \( F, G \) is written as
\begin{equation*}
\int_V F d^k\Bx \lrboldpartial G
\equiv
\int_V \lr{ F d^k\Bx \lboldpartial} G
+
\int_V F d^k\Bx \lr{ \rboldpartial G }.
\end{equation*}
The explicit meaning of these directional acting derivative operations is given by the chain rule coordinate expansion
\begin{dmath*}
F d^k \Bx \lrboldpartial G
=
F d^k \Bx \lr{ \sum_i \Bx^i {\stackrel{ \leftrightarrow }{\partial_i}} } G
=
(\partial_i F) d^k \Bx \sum_i \Bx^i G
+
F d^k \Bx \sum_i \Bx^i (\partial_i G)
\equiv
(F d^k \Bx \lboldpartial) G
+
F d^k \Bx (\rboldpartial G),
\end{dmath*}
with \( \boldpartial \) acting on \( F \) and \( G \), but not the volume element \( d^k \Bx \), which may also be a function of the implied parameterization.
} % definition

The vector derivative
% (and gradient)
may not commute with \( F, G \) nor the volume element \( d^k \Bx \), so we are forced to use some notation to indicate what the vector derivative (or gradient) acts on.
In conventional right acting cases, where there is no ambiguity, arrows will usually be omitted, but braces may also be used to indicate the scope of derivative operators.
This bidirectional notation will also be used for the gradient, especially for volume integrals in \R{3} where the vector derivative is identical to the gradient.

Some authors use the Hestenes dot notation, with overdots or primes to indicating the exact scope of multivector derivative operators, as in
\begin{dmath}\label{eqn:fundamentalTheoremOfCalculus:260}
\dot{F} d^k \Bx \dot{\boldpartial} \dot{G} =
\dot{F} d^k \Bx \dot{\boldpartial} G
+
F d^k \Bx \dot{\boldpartial} \dot{G}.
\end{dmath}
The dot notation has the advantage of emphasizing that the action of the vector derivative (or gradient) is on the functions \( F, G \), and not on the hypervolume element \( d^k \Bx \).
However, in this book, where primed operators such as \( \spacegrad' \) are used to indicate that derivatives are taken with respect to primed \( \Bx' \) variables, a mix of dots and ticks would have been confusing.
%Over arrows also have the advantage of being visually conspicuous.

\subsection{Fundamental theorem.}
\index{fundamental theorem of geometric calculus}

The fundamental theorem of geometric calculus is a generalization of many conventional scalar and vector integral theorems, and relates a hypervolume integral to its boundary.
This is a a powerful theorem, which we will use with Green's functions to solve Maxwell's equation, but also to derive the geometric algebra form of Stokes' theorem, from which most of the familiar integral calculus results follow.
\input{Theorem_fundamental_theorem_of_gc.tex}
The geometry of the hypersurface element \( d^{k-1} \Bx \) will be made more clear when we
consider the specific cases of \( k = 1, 2, 3 \), representing generalized line, surface, and volume integrals respectively.
Instead of terrorizing the reader with a general proof
\cref{thm:fundamentalTheoremOfCalculus:1},
which requires some unpleasant index gymnastics,
this book
will separately state and prove the fundamental theorem of calculus
for each of the \( k = 1, 2, 3 \) cases that are of interest for problems in \R{2} and \R{3}.
For the interested reader, a sketch of the general proof
of \cref{thm:fundamentalTheoremOfCalculus:1}
is available in \cref{chap:gagcProof}.

Before moving on to the line, surface, and volume integral cases, we will state and prove the
general Stokes' theorem in its geometric algebra form.

%}
%\EndArticle
