%
% Copyright � 2018 Peeter Joot.  All Rights Reserved.
% Licenced as described in the file LICENSE under the root directory of this GIT repository.
%
%{
%%%\input{../latex/blogpost.tex}
%%%\renewcommand{\basename}{volumeintegral}
%%%%\renewcommand{\dirname}{notes/phy1520/}
%%%\renewcommand{\dirname}{notes/ece1228-electromagnetic-theory/}
%%%%\newcommand{\dateintitle}{}
%%%%\newcommand{\keywords}{}
%%%
%%%\input{../latex/peeter_prologue_print2.tex}
%%%
%%%\usepackage{peeters_layout_exercise}
%%%\usepackage{peeters_braket}
%%%\usepackage{peeters_figures}
%%%\usepackage{siunitx}
%%%%\usepackage{mhchem} % \ce{}
%%%%\usepackage{macros_bm} % \bcM
%%%%\usepackage{macros_qed} % \qedmarker
%%%\usepackage{txfonts} % \ointclockwise
%%%
%%%\beginArtNoToc
%%%
%%%\generatetitle{Volume integral.}
%%%%\chapter{Volume integral.}
\label{chap:volumeintegral}

%\subsection{Volume integral.}
\index{volume parameterization}
\index{volume element}
\index{differential form}
A three parameter curve, and the corresponding differentials with respect to those parameters, is sketched in
\cref{fig:normalsOnVolumeAreaElement:normalsOnVolumeAreaElementFig11}.

\imageFigure{../figures/gabook/normalsOnVolumeAreaElementFig11}{Three parameter volume element.}{fig:normalsOnVolumeAreaElement:normalsOnVolumeAreaElementFig11}{0.4}

Given parameters \( u_1, u_2, u_3 \), we can denote the differentials along each of the parameterization directions as
\begin{dmath}\label{eqn:volumeintegral:100}
\begin{aligned}
d\Bx_1 &= \PD{u_1}{\Bx} du_1 = \Bx_1 du_1 \\
d\Bx_2 &= \PD{u_2}{\Bx} du_2 = \Bx_2 du_2 \\
d\Bx_3 &= \PD{u_3}{\Bx} du_3 = \Bx_3 du_3.
\end{aligned}
\end{dmath}

The trivector valued volume element for this parameterization is
\begin{equation}\label{eqn:volumeintegral:120}
d^3 \Bx
=
d\Bx_1 \wedge
d\Bx_1 \wedge
d\Bx_1
=
d^3 u\, (\Bx_1 \wedge \Bx_2 \wedge \Bx_3),
\end{equation}
where \( d^3 u = du_1 du_2 du_3 \).
The vector derivative, the projection of the gradient onto the volume at the point of integration (also called the tangent space), now has three components
\begin{dmath}\label{eqn:volumeintegral:200}
\boldpartial
=
\sum_i \Bx^i (\Bx_i \cdot \spacegrad)
=
\Bx^1 \PD{u_1}{}
+
\Bx^2 \PD{u_2}{}
+
\Bx^3 \PD{u_3}{}
\equiv
\Bx^1 \partial_1
+
\Bx^2 \partial_2
+
\Bx^3 \partial_3.
\end{dmath}

The volume integral specialization of \cref{dfn:fundamentalTheoremOfCalculus:240} can now be stated

\makedefinition{Multivector volume integral.}{dfn:volumeintegral:100}{
Given an connected volume \( V \) parameterized by two parameters, and multivector functions \( F, G \), we define the volume integral as
\begin{equation*}
\int_V F d^3\Bx \lrboldpartial G
\equiv
\int_V \lr{ F d^3\Bx \lboldpartial} G
+
\int_V F d^3\Bx \lr{ \rboldpartial G },
\end{equation*}
where the three parameter differential form \( d^3 \Bx = d^3 u\, \Bx_1 \wedge \Bx_2 \wedge \Bx_3, d^3 u = du_1 du_2 du_3 \) varies over the volume, and \( \lrboldpartial \) acts on \( F, G \), but not the volume element \( d^2 \Bx \).
} % definition

The volume integral specialization of \cref{thm:fundamentalTheoremOfCalculus:1} is

\input{Theorem_multivector_volume_integral.tex}
To see why this works, and define \( d^2 \Bx \) more precisely, we would first like to reduce the product of the volume element and the vector derivative
\begin{dmath}\label{eqn:volumeintegral:300}
d^3\Bx \boldpartial
=
d^3 u\, \lr{ \Bx_1 \wedge \Bx_2 \wedge \Bx_3 } \lr{ \Bx^1 \partial_1 + \Bx^2 \partial_2 + \Bx^3 \partial_3 }.
\end{dmath}

Since all \( \Bx^i \) lie within \( \Span \setlr{ \Bx_1, \Bx_2, \Bx_3 } \), this multivector product has only a vector grade.  That is
\begin{dmath}\label{eqn:volumeintegral:320}
\lr{ \Bx_1 \wedge \Bx_2 \wedge \Bx_3 } \Bx^i
=
\lr{ \Bx_1 \wedge \Bx_2 \wedge \Bx_3 } \cdot \Bx^i
+
\cancel{ \lr{ \Bx_1 \wedge \Bx_2 \wedge \Bx_3 } \wedge \Bx^i },
\end{dmath}
for all \( \Bx^i \).  These products reduces to
\begin{dmath}\label{eqn:volumeintegral:1621}
\begin{aligned}
\lr{ \Bx_2 \wedge \Bx_3 \wedge \Bx_1 } \Bx^1 &= \Bx_2 \wedge \Bx_3 \\
\lr{ \Bx_3 \wedge \Bx_1 \wedge \Bx_2 } \Bx^2 &= \Bx_3 \wedge \Bx_1 \\
\lr{ \Bx_1 \wedge \Bx_2 \wedge \Bx_3 } \Bx^3 &= \Bx_1 \wedge \Bx_2.
\end{aligned}
\end{dmath}

Inserting \cref{eqn:volumeintegral:1621}
into the volume integral, we find
\begin{dmath}\label{eqn:volumeintegral:380}
\int_V F d^3\Bx \boldpartial G
=
\int_V \lr{ F d^3\Bx \lboldpartial} G
+
\int_V F d^3\Bx \lr{ \rboldpartial G }
=
\int_V d^3 u\, \lr{
   (\partial_1 F) \Bx_2 \wedge \Bx_3 G
   +
   (\partial_2 F) \Bx_3 \wedge \Bx_1 G
   +
   (\partial_3 F) \Bx_1 \wedge \Bx_2 G
}
+
\int_V d^3 u\, \lr{
   F \Bx_2 \wedge \Bx_3 (\partial_1 G)
   +
   F \Bx_3 \wedge \Bx_1 (\partial_2 G)
   +
   F \Bx_1 \wedge \Bx_2 (\partial_3 G)
}
=
\int_V d^3 u\, \lr{
   \partial_1 (F \Bx_2 \wedge \Bx_3 G)
   +
   \partial_2 (F \Bx_3 \wedge \Bx_1 G)
   +
   \partial_3 (F \Bx_1 \wedge \Bx_2 G)
}
-
\int_V d^3 u\, \lr{
   F (\partial_1 (\Bx_2 \wedge \Bx_3)) G
   +
   F (\partial_2 (\Bx_3 \wedge \Bx_1)) G
   +
   F (\partial_3 (\Bx_1 \wedge \Bx_2)) G
}
=
\int_V d^3 u\, \lr{
   \partial_1 (F \Bx_2 \wedge \Bx_3 G)
   +
   \partial_2 (F \Bx_3 \wedge \Bx_1 G)
   +
   \partial_3 (F \Bx_1 \wedge \Bx_2 G)
}
-
\int_V d^3 u\, F
\lr{
   \partial_1 (\Bx_2 \wedge \Bx_3)
   +
   \partial_2 (\Bx_3 \wedge \Bx_1)
   +
   \partial_3 (\Bx_1 \wedge \Bx_2)
}
G
.
\end{dmath}

The sum within the second integral is
\begin{dmath}\label{eqn:volumeintegral:1111}
\begin{aligned}
\sum_{i = 1}^3 \partial_i \lr{ I_k \cdot \Bx^i }
&=
\partial_3 \lr{ (\Bx_1 \wedge \Bx_2 \wedge \Bx_3) \cdot \Bx^3 }
+
\partial_1 \lr{ (\Bx_2 \wedge \Bx_3 \wedge \Bx_1) \cdot \Bx^1 }
+
\partial_2 \lr{ (\Bx_3 \wedge \Bx_1 \wedge \Bx_2) \cdot \Bx^2 } \\
&=
\partial_3 \lr{ \Bx_1 \wedge \Bx_2 }
+
\partial_1 \lr{ \Bx_2 \wedge \Bx_3 }
+
\partial_2 \lr{ \Bx_3 \wedge \Bx_1 } \\
&=
         (\partial_3 \Bx_1) \wedge \Bx_2 + \Bx_1 \wedge (\partial_3 \Bx_2) \\
&\quad + (\partial_1 \Bx_2) \wedge \Bx_3 + \Bx_2 \wedge (\partial_1 \Bx_3) \\
&\quad + (\partial_2 \Bx_3) \wedge \Bx_1 + \Bx_3 \wedge (\partial_2 \Bx_1) \\
&=
\Bx_2 \wedge \lr{ - \partial_3 \Bx_1 + \partial_1 \Bx_3 }
+
\Bx_3 \wedge \lr{ - \partial_1 \Bx_2 + \partial_2 \Bx_1 }
+
\Bx_1 \wedge \lr{ - \partial_2 \Bx_3 + \partial_3 \Bx_2 } \\
&=
\Bx_2 \wedge \lr{ - \frac{\partial^2 \Bx}{\partial_3 \partial_1} + \frac{\partial^2 \Bx}{\partial_1 \partial_3} }
+
\Bx_3 \wedge \lr{ - \frac{\partial^2 \Bx}{\partial_1 \partial_2} + \frac{\partial^2 \Bx}{\partial_2 \partial_1} }
+
\Bx_1 \wedge \lr{ - \frac{\partial^2 \Bx}{\partial_2 \partial_3} + \frac{\partial^2 \Bx}{\partial_3 \partial_2} },
\end{aligned}
\end{dmath}
which is zero by equality of mixed partials.
This leaves three perfect differentials, which can integrated separately, giving
\begin{dmath}\label{eqn:volumeintegral:400}
\int_V F d^3\Bx \boldpartial G
=
\int du_2 du_3
\evalbar{ \lr{ F \Bx_2 \wedge \Bx_3 G } }{\Delta u_1}
+
\int du_3 du_1
\evalbar{ \lr{ F \Bx_3 \wedge \Bx_1 G } }{\Delta u_2}
+
\int du_1 du_2
\evalbar{ \lr{ F \Bx_1 \wedge \Bx_2 G } }{\Delta u_3}
=
\int
\evalbar{ \lr{ F d\Bx_2 \wedge d\Bx_3 G } }{\Delta u_1}
+
\int
\evalbar{ \lr{ F d\Bx_3 \wedge d\Bx_1 G } }{\Delta u_2}
+
\int
\evalbar{ \lr{ F d\Bx_1 \wedge d\Bx_2 G } }{\Delta u_3}.
\end{dmath}

This proves the theorem from an algebraic point of view.
With the aid of a geometrical model, such as that of \cref{fig:differentialVolume:differentialVolumeFig}, if
assuming that \( d\Bx_1, d\Bx_2, d\Bx_3 \) is a right handed triple).
it is possible to convince oneself that the two parameter integrands describe an integral over a counterclockwise oriented surface (
\imageTwoFigures{../figures/GAelectrodynamics/differentialVolumeFig1}{../figures/GAelectrodynamics/differentialVolumeFig2}{Differential surface of a volume.}{fig:differentialVolume:differentialVolumeFig}{scale=0.05}

We obtain the RHS of \cref{thm:volumeintegral:100} if we
introduce a mnemonic for the bounding oriented surface of the volume
\begin{dmath}\label{eqn:volumeintegral:1641}
d^2 \Bx \equiv d\Bx_1 \wedge d\Bx_2 + d\Bx_2 \wedge d\Bx_3 + d\Bx_3 \wedge d\Bx_1,
\end{dmath}
where it is implied that each component of this area element and anything that it is multiplied with is evaluated on the boundaries of the integration volume (for the parameter omitted) as detailed explicitly in
\cref{eqn:volumeintegral:400}.

\subsection{Three parameter Stokes' theorem.}

Three special cases of \cref{thm:volumeintegral:100} can be obtained by integrating scalar, vector or bivector functions over the volume, as follows

\maketheorem{Volume integral of scalar function (Stokes').}{thm:volumeintegral:420}{
Given a scalar function \( f(\Bx) \) its volume integral is given by
\begin{equation*}
\int_V d^3 \Bx \cdot \boldpartial f =
\int_V d^3 \Bx \cdot \spacegrad f = \ointctrclockwise_{\partial V} d^2\Bx f.
\end{equation*}
In \R{3} this can be written as
\begin{equation*}
\int_V dV \spacegrad f = \int_{\partial V} dA \ncap f
\end{equation*}
where \( \ncap \) is the outwards normal specified by \( d^2 \Bx = I \ncap dA, \) and \( d^3 \Bx = I dV \).
} % theorem

\input{Theorem_volume_integral_of_vector_stokes.tex}

\input{Theorem_volume_integral_of_bivector_stokes_divergence.tex}

\subsection{Divergence theorem.}

Observe that for \R{3} we there are dot product relations in each of
\cref{thm:volumeintegral:420},
\cref{thm:volumeintegral:1661} and
\cref{thm:volumeintegral:1681} which can be summarized as
\index{divergence theorem}
\input{Theorem_divergence.tex}

%}
%\EndNoBibArticle
