%
% Copyright © 2017 Peeter Joot.  All Rights Reserved.
% Licenced as described in the file LICENSE under the root directory of this GIT repository.
%
%{
Geometric algebra takes a vector space and adds two additional operations, a vector multiplication operation, and a generalized addition operation that extends vector addition to include addition of scalars and products of vectors.
Multiplication of vectors is indicated by juxtaposition, for example, if \( \Bx, \By, \Be_1, \Be_2, \Be_3, \cdots \) are vectors, then some vector products are
\begin{dmath}\label{eqn:multivector:20}
\begin{aligned}
&\Bx \By, \Bx \By \Bx, \Bx \By \Bx \By, \\
&\Be_1 \Be_2, \Be_2 \Be_1, \Be_2 \Be_3, \Be_3 \Be_2, \Be_3 \Be_1, \Be_1 \Be_3, \\
&\Be_1 \Be_2 \Be_3, \Be_3 \Be_1 \Be_2, \Be_2 \Be_3 \Be_1, \Be_3 \Be_2 \Be_1, \Be_2 \Be_1 \Be_3, \Be_1 \Be_3 \Be_2, \\
&\Be_1 \Be_2 \Be_3 \Be_1, \Be_1 \Be_2 \Be_3 \Be_1 \Be_3 \Be_2, \cdots
\end{aligned}
\end{dmath}

Vector multiplication is constrained by a rule, called the contraction axiom, that gives a meaning to the square of a vector
\boxedEquation{eqn:multivector:120}{
\Bx \Bx \equiv \Bx \cdot \Bx.
}

The square of a vector, by this definition, is the squared length of the vector, and is a scalar.
This may not appear to be a useful way to assign meaning to the simplest of vector products, since the product and the vector live in separate spaces.
If we want a closed algebraic system that includes both vectors and their products, we have to allow for the addition of scalars, vectors, or any products of vectors.  Such a sum is called a multivector, an example of which is
\begin{dmath}\label{eqn:multivector:40}
1 + 2 \Be_1 + 3 \Be_1 \Be_2 + 4 \Be_1 \Be_2 \Be_3.
\end{dmath}
In this example, we have added a
scalar (or 0-vector) \( 1 \), to a
vector (or 1-vector) \( 2 \Be_1 \), to a
bivector (or 2-vector) \( 3 \Be_1 \Be_2 \), to a
trivector (or 3-vector) \( 4 \Be_1 \Be_2 \Be_3 \).
Geometric algebra uses vector multiplication to build up a hierarchy of geometrical objects, representing points, lines, planes, volumes and hypervolumes (in higher dimensional spaces.)

\index{scalar}
\index{0-vector}
\paragraph{Scalar.}
A scalar, which we will also call a 0-vector, is a zero-dimensional object with sign, and a magnitude.
We may geometrically interpret a scalar as a (signed) point in space.
%The sign of a scalar can be represented graphically as an arrow with a head and a tail pointing into the paper (or chalkboard),
%as illustrated in
%\cref{fig:scalarOrientation:scalarOrientationFig1} where a crossed circle represents the tail, and a solid dot represents the head.
%%\footnote{We don't usually try to represent the magnitude of a scalar graphically, but could do so by scaling the size of the cross or dot.}
%\imageFigure{../figures/GAelectrodynamics/scalarOrientationFig1}{Scalar illustration.}{fig:scalarOrientation:scalarOrientationFig1}{0.05}

\index{vector}
\index{1-vector}
\paragraph{Vector.}
A vector, which we will also call a 1-vector, is a one-dimensional object with a sign, a magnitude, and a rotational attitude within the space it is embedded.
%, and possible orientation, as illustrated in
%\cref{fig:VectorsWithOppositeOrientation:VectorsWithOppositeOrientationFig1}, where the length of the line segment represents the magnitude, and
%the sign of the vector can be represented graphically using the relative placement of the head vs. tail of the vector.
When vectors are negated the relative placement of the ``head'' vs. the ``tail'' are toggled as illustrated in \cref{fig:VectorsWithOppositeOrientation:VectorsWithOppositeOrientationFig1}.
\imageFigure{../figures/GAelectrodynamics/VectorsWithOppositeOrientationFig1}{Vector illustration.}{fig:VectorsWithOppositeOrientation:VectorsWithOppositeOrientationFig1}{0.3}
%One vector in isolation is a one dimensional object, however, when embedded in a higher order space, such as a plane or a volume, it also has an orientation in that space.

Vectors can be added graphically by connecting them head to tail in sequence, and joining the first tail point to the final head, as
illustrated in
\cref{fig:vectorAddition:vectorAdditionFig1}.
\imageFigure{../figures/GAelectrodynamics/vectorAdditionFig1}{Graphical vector addition.}{fig:vectorAddition:vectorAdditionFig1}{0.3}

\index{bivector}
\index{2-vector}
\paragraph{Bivector.}

We now wish to define a bivector, or 2-vector, as a 2 dimensional object representing a signed plane segment with magnitude and orientation.  Formally,
assuming a vector product, the algebraic description of a bivector is

\makedefinition{Bivector.}{dfn:multivector:60}{
A bivector, or 2-vector, is a sum of products of pairs of normal vectors.
Given an \( N \) dimensional vector space \( V \) with an orthonormal basis \( \setlr{ \Be_1, \Be_2, \cdots, \Be_N } \),
a general bivector can be expressed as
\begin{equation*}
\sum_{1 \le i < j \le N} B_{ij} \Be_i \Be_j,
\end{equation*}
where \( B_{ij} \) is a scalar.
The vector basis \( V \) is said to be a generator of a bivector space.
} % definition

The bivector provides a structure that can encode plane oriented quantities such as torque, angular momentum, or a general plane of rotation.
A quantity like angular momentum can be represented as a magnitude times a quantity that represents the orientation of the plane of rotation.
In conventional vector algebra we use the normal of the plane to describe this orientation, but that is problematic in higher dimensional spaces where there is no unique normal.
Use of the normal to represent a plane is also logically problematic in two dimensional spaces, which have to be extended to three dimensions to use normal centric constructs like the cross product.
A bivector representation of a plane can eliminate the requirement utilize a third (normal) dimension, which may not be relevant in the problem, and can allow some concepts (like the cross product) to be generalized to dimensions other than three when desirable.

One of the implications of the contraction axiom \cref{eqn:multivector:120}, to be discussed in more detail a bit later, is a linear dependence between bivectors formed from normal products.  For example, given any pair of unit bivectors, where \( i \ne j \) we have
\begin{dmath}\label{eqn:multivector:140}
\Be_i \Be_j + \Be_j \Be_i = 0,
\end{dmath}
This is why the sum in \cref{dfn:multivector:60} was over only half the possible pairs of \( i \ne j \) indexes.
The reader can check that the set of all bivectors is a vector space per
\cref{def:prerequisites:vectorspace}, so we will call the set of all bivectors a bivector space.
In \R{2} a basis for the bivector space is \( \setlr{ \Be_1 \Be_2 } \), whereas in \R{3} a basis for the bivector space is
\( \setlr{ \Be_1 \Be_2, \Be_2 \Be_3, \Be_3 \Be_1 } \).  The unit bivectors for two possible \R{3} bivector space bases are illustrated in
\cref{fig:unitBivectors:unitBivectorsFig}.
\imageTwoFigures
{../figures/GAelectrodynamics/unitBivectorsFig1}
{../figures/GAelectrodynamics/unitBivectorsFig2}
{Unit bivectors for \R{3}}
{fig:unitBivectors:unitBivectorsFig}
{scale=0.35}

We interpret the sign of a vector as an indication of the sense of the vector's ``head'' vs ``tail''.
For a bivector, we can interpret the sign as a representation of a
a ``top'' vs. ``bottom'', or equivalently a left or right ``handedness'', as illustrated using arrows around a plane segment in
\cref{fig:circularBivectorsIn3D:circularBivectorsIn3DFig1}.
\imageFigure{../figures/GAelectrodynamics/circularBivectorsIn3DFig1}{Circular representation of two bivectors.}{fig:circularBivectorsIn3D:circularBivectorsIn3DFig1}{0.3}
For a product like \( \Be_1 \Be_2 \), the sense of the handedness follows the path \( 0 \rightarrow \Be_1 \rightarrow \Be_1 + \Be_2 \rightarrow \Be_2 \rightarrow 0 \) around the unit square in the x-y plane.
This is illustrated for all the unit bivectors in \cref{fig:unitBivectors:unitBivectorsFig}.
In \R{3} we can use the right hand rule to visualize such a handedness.  You could say that we are using the direction of the fingers around the normal to indicate the sign of the bivector, but without actually drawing that normal.

Similar to the interpretation of the magnitude of a vector as the length of that vector, we interpret the magnitude of a bivector (to be defined more exactly later), as the area of the bivector.
Other than having a boundary that surrounds a given area, a graphical bivector representation as a plane segment need not have any specific geometry, which is illustrated in
\cref{fig:bivectorRepresentationsInPlane:bivectorRepresentationsInPlaneFig1} for a set of bivectors all representing \( \Be_1 \Be_2 \).
\imageFigure{../figures/GAelectrodynamics/bivectorRepresentationsInPlaneFig1}{Graphical representations of \( \Be_1 \Be_2 \).}{fig:bivectorRepresentationsInPlane:bivectorRepresentationsInPlaneFig1}{0.3}

An oriented plane segment can always be represented as a bivector in any number of dimensions, however, when the generating vector space has dimension \( N \ge 4 \) not all bivectors defined by \cref{dfn:multivector:60} necessarily represent oriented plane segments.
The restrictions required for a bivector to have an associated oriented plane segment interpretation in higher dimensional spaces will be defined later.

Vector addition can be performed graphically by connecting vectors head to tail, and joining the first tail to the last head.  A similar procedure can be used for bivector addition as well, but gets complicated if the bivectors lie in different planes.  Here is a simple bivector sum
\begin{dmath}\label{eqn:multivector:160}
3 \Be_1 \Be_2 - 2 \Be_1 \Be_2 + 5 \Be_1 \Be_2 = 6 \Be_1 \Be_2,
\end{dmath}
which can be interpreted as taking a 3 unit area, subtracting a 2 unit area, and adding a 5 unit area.  This sum is illustrated in
\cref{fig:bivectorAdditionInPlane:bivectorAdditionInPlaneFig1}.
An visualization of arbitrarily oriented bivector addition can be found in
\cref{fig:AdditionOfBivectors:AdditionOfBivectorsFig2}, where \( \text{blue} + \text{red} = \text{green} \).  This visualization shows that the
moral of the story is that we will almost exclusively be adding bivectors algebraically, but can interpret the sum geometrically after the fact.
\imageFigure{../figures/GAelectrodynamics/bivectorAdditionInPlaneFig1}{Graphical representation of bivector addition in plane.}{fig:bivectorAdditionInPlane:bivectorAdditionInPlaneFig1}{0.2}
\imageFigure{../figures/GAelectrodynamics/AdditionOfBivectorsFig2}{Bivector addition.}{fig:AdditionOfBivectors:AdditionOfBivectorsFig2}{0.3}
%The same can be done with bivectors, where the bivectors are also connected with compatible orientation to construct a sum.
%This is illustrated graphically in \cref{fig:AdditionOfBivectors:AdditionOfBivectorsFig1}, where a blue bivector with a right handed orientation is added to a red bivector with right handed orientation, to form a green bivector also with right handed orientation, where all orientations are with respect to the exterior of the bounding surface formed by the three bivectors.
%\imageFigure{../figures/GAelectrodynamics/AdditionOfBivectorsFig1}{Bivector addition.}{fig:AdditionOfBivectors:AdditionOfBivectorsFig1}{0.3}

\index{trivector}
\index{3-vector}
\paragraph{Trivector.}

Again, assuming a vector product

\makedefinition{Trivector.}{dfn:multivector:80}{
A trivector, or 3-vector, is a sum of products of triplets of mutually normal vectors.
Given an \( N \) dimensional vector space \( V \) with an orthonormal basis \( \setlr{ \Be_1, \Be_2, \cdots, \Be_N } \), a trivector is any value
\begin{equation*}
\sum_{1 \le i < j < k \le N} T_{ijk} \Be_i \Be_j \Be_k,
\end{equation*}
where \( \T_{ijk} \) is a scalar.
The vector space \( V \) is said to generate a trivector space.
} % definition

In \R{3} all trivectors are scalar multiples of \( \Be_1 \Be_2 \Be_3 \).
Like scalars, there is no direction to such a quantity, but like scalars trivectors may be signed.  The magnitude of a trivector may be interpreted as a volume.
We will defer interpreting the sign of a trivector geometrically until we tackle integration theory.
%%%, which requires some interpretation.
%%%We can interpret the magnitude of a trivector as a volume, but what is a signed volume?
%%%One answer to this question is that we can interpret the sign of the volume as the exterior or the interior of the surface on the boundry of the volume.
%%%We will see another answer when we study integration theory, since geometric integration theory uses signed volume elements, and
%%%swapping the order of two adjacent products in the volume element toggles the sign.
%%%\footnote{In conventional integration theory,
%%%this sign change occurs when swapping rows or columns in the Jacobian, but this is masked by taking the absolute value of the Jacobian after coordinate transformation.}
%%%One possible interpretation of this sign is the interior or the exterior of the bounding surface of a volume.
%%%%This orientation can be visualized with a normal pointing into or out of the volume, or like bivectors, with a cyclic direction on the surface of the volume as in illustrated with the spherical volume of \cref{fig:orientedVolume:orientedVolumeFig1}.
%%%%\imageFigure{../figures/GAelectrodynamics/orientedVolumeFig1}{Oriented Volume}{fig:orientedVolume:orientedVolumeFig1}{0.3}
%%%%In greater than three dimensions, a trivector can have a ``direction'' in the higher dimensional space, as well as a sidedness.
%%%%As was the case with the bivector, because not all the products \( \Be_i \Be_j \Be_k \) for any set of indexes \( i, j, k \) are independent, it is possible to form a trivector as a sum over a more restricted set, such as \( \sum_{1 \le i < j < k \le N} T_{ijk} \Be_i \Be_j \Be_k \).
%%%%In particular, in three dimensions, all trivectors can be expressed as scalar multiples of \( \Be_1 \Be_2 \Be_3 \).
%%%%
\index{k-vector}
\index{grade}
\paragraph{K-vector.}
\makedefinition{K-vector and grade.}{dfn:multivector:100}{
A k-vector is a sum of products of \( k \) mutually normal vectors.
Given an \( N \) dimensional vector space with an orthonormal basis \( \setlr{ \Be_1, \Be_2, \cdots, \Be_N } \),
a general k-vector can be expressed as
\begin{equation*}
\sum_{1 \le i < j \cdots < m \le N} K_{i j \cdots m} \Be_{i} \Be_{j} \cdots \Be_{m},
\end{equation*}
where \( K_{i j \cdots m} \) is a scalar, indexed by \( k \) indexes \( i, j, \cdots, m \).

The number \( k \) of normal vectors that generate a k-vector is called the grade.

A 1-vector is defined as a vector, and a 0-vector is defined as a scalar.

The vector space \( V \) is said to generate the k-vector space.
} % definition

We will see that the highest grade for a k-vector in an N dimensional vector space is \( N \).

\index{multivector}
\index{multivector space}
\paragraph{Multivector space.}
\makedefinition{Multivector space.}{def:multiplication:multivectorspace}{
   Given an N dimensional (generating) vector space \( V \) with an orthonormal basis \( \setlr{ \Be_1, \Be_2, \cdots, \Be_N } \),
%a basis \( \setlr{ \Bx_1, \Bx_2, \cdots } \),
and a vector multiplication operation represented by juxtaposition,
a multivector is a sum of k-vectors, \( k \in [ 1, N ] \), such as
   \( a_0 + \sum_i a_i \Be_i + \sum_{i \ne j} a_{ij} \Be_i \Be_j + \sum_{i \ne j \ne k} a_{ijk} \Be_i \Be_j \Be_k + \cdots \), where \( a_0, a_i, a_{ij}, \cdots \) are scalars.

The multivector space generated by \( V \) is a set \( M = \setlr{ x, y, z, \cdots } \) of multivectors, where the following axioms are satisfied

\begin{tcolorbox}[tab2,tabularx={X|Y},title=Multivector space axioms.,boxrule=0.5pt]
    Contraction. & \( \Bx^2 = \Bx \cdot \Bx, \,\forall \Bx \in V \) \\ \hline
    Addition is closed. & \( x + y \in M \) \\ \hline
    Multiplication is closed. & \( x y \in M \) \\ \hline
    Addition is associative. & \( (x + y) + z = x + (y + z) \) \\ \hline
    Addition is commutative. & \( y + x = x + y \) \\ \hline
    There exists a zero element \( 0 \in M \).  & \( x + 0 = x \) \\ \hline
    There exists a negative additive inverse \( -x \in M \). & \( x + (-x) = 0 \) \\ \hline
    Multiplication is distributive.  & \( x( y + z ) = x y + x z \), \( (x + y)z = x z + y z \) \\ \hline
    Multiplication is associative. & \( (x y) z = x ( y z ) \) \\ \hline
    There exists a multiplicative identity \( 1 \). & \( 1 x = x \) \\ \hline
\end{tcolorbox}
}

Compared to the vector space, def'n. \ref{def:prerequisites:vectorspace}, the multivector space

\begin{itemize}
\item presumes a vector multiplication operation, which is not assumed to be commutative (order matters),
\item generalizes vector addition to multivector addition,
\item generalizes scalar multiplication to multivector multiplication (of which scalar multiplication and vector multiplication are special cases),
\item and most importantly, specifies a rule providing the meaning of a squared vector (the contraction axiom).
\end{itemize}

The contraction axiom is arguably the most important of the multivector space axioms, as it allows for multiplicative closure without an infinite dimensional multivector space.
The remaining set of non-contraction axioms of a multivector space are almost that of a field
\footnote{A mathematician would call a multivector space a non-commutative ring with identity \citep{van1943modern}, and could state the multivector space definition much more compactly without listing all the properties of a ring explicitly as done above.}
(as encountered in the study of complex inner products),
as they describe most of the properties one
would expect of a ``well behaved'' set of number-like quantities.
However, a field also requires a multiplicative inverse element for all elements of the space, which exists for some multivector subspaces, but not in general.

%These axioms may seem simple enough, especially since they are not that different from the familiar axioms of the vector space,
%but it will take considerable work to extract all their consequences.
%The subject of Geometric Algebra can be viewed as the study of the impliciations of the axioms
%of the multivector space.

%}
