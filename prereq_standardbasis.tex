%
% Copyright © 2017 Peeter Joot.  All Rights Reserved.
% Licenced as described in the file LICENSE under the root directory of this GIT repository.
%
\index{dot product}
\makedefinition{Dot product.}{dfn:prerequisites:dotproduct}{
Let \( \Bx, \By \) be vectors from a vector space \( V \).
A dot product \( \Bx \cdot \By \) is a mapping \( V \cross V \rightarrow \bbR \)
with the following properties

\begin{tcolorbox}[tab2,tabularx={X|Y},title=Dot product properties.,boxrule=0.5pt]
    Symmetric in both arguments & \( \Bx \cdot \By = \By \cdot \Bx \) \\ \hline
    Bilinear & \( (a \Bx + b \By) \cdot (a' \Bx' + b' \By' ) =
a a' (\Bx \cdot \Bx') + b b' (\By \cdot \By')
+
a b' (\Bx \cdot \By') + b a' (\By \cdot \Bx') \)
\\ \hline
    (Optional) Positive length & \( \Bx \cdot \Bx > 0, \Bx \ne 0 \) \\ \hline
\end{tcolorbox}
} % definition

Because the dot product is bilinear, it is
specified completely by the dot products of a set of basis elements for the space.
For example,
given a basis \( \setlr{ \Be_1, \Be_2, \cdots, \Be_N} \), and two vectors
\begin{dmath}\label{eqn:prereq_standardbasis:240}
\begin{aligned}
   \Bx &= \sum_{i = 1}^N x_i \Be_i \\
   \By &= \sum_{i = 1}^N y_i \Be_i,
\end{aligned}
\end{dmath}
the dot product of the two is
\begin{dmath}\label{eqn:prereq_standardbasis:260}
\Bx \cdot \By
=
   \lr{ \sum_{i = 1}^N x_i \Be_i } \cdot
   \lr{ \sum_{j = 1}^N y_j \Be_j }
=
   \sum_{i,j = 1}^N x_i y_j \lr{ \Be_i \cdot \Be_j }.
\end{dmath}
Such an expansion in coordinates can be written in matrix form as
\begin{dmath}\label{eqn:prereq_standardbasis:280}
\Bx \cdot \By
=
\Bx^\T G \By,
\end{dmath}
where \( G \) is the symmetric matrix with elements \( g_{ij} = \Be_i \cdot \Be_j \).
This matrix \( G \), or its elements \( g_{ij} \) is also called the metric for the space.

For Euclidean spaces, which are the primary focus of this book, the metric is not only diagonal, but is the identity matrix.
Slightly more general metrics are of interest in electrodynamics.  In particular, a four dimensional (relativistic) vector space, where the metric has diagonals \( (1,-1,-1,-1) \) or \( (-1, 1,1,1) \) allows for the construction of a geometric algebra that allows Maxwell's equations to take their very simplest form.  Such a metric does not have the (Euclidean) positive definite property \( \Bx^\T G \Bx > 0, \Bx \ne 0 \).

\index{length}
\makedefinition{Length}{dfn:prerequisites:norm}{
   The squared norm of a vector \( \Bx \) is defined as
\begin{equation*}
   \Norm{\Bx}^2 = \Bx \cdot \Bx,
\end{equation*}
a quantity that need not be positive.
The length of a vector \( \Bx \) is defined as
\begin{equation*}
\Norm{\Bx} =
\sqrt{\Abs{ \Bx \cdot \Bx }}.
\end{equation*}
}

%A vector space with an associated norm based length is called a normed vector space.
%Any dot product space is also a normed vector space.

\index{unit vector}
\makedefinition{Unit vector}{dfn:prerequisites:unitvector}{
   A vector \( \Bx \) is called a unit vector if its absolute squared norm is one (\( \Abs{\Bx \cdot \Bx} = 1 \)).
} % definition

%A unit vector \( \xcap \) may be generated from any vector \( \Bx \) that has a non-zero squared norm by computing
%
%\begin{dmath}\label{eqn:prereq_standardbasis:220}
%\xcap = \frac{\Bx}{\sqrt{\Abs{\Norm{\Bx}^2}}}.
%\end{dmath}
%
\index{normal}
\makedefinition{Normal}{dfn:prerequisites:normal}{
   Two vectors from a vector space \( V \) are normal, or orthogonal, if their dot product is zero, \( \Bx \cdot \By = 0 \,\forall \Bx, \By \in V \).
}

\index{orthonormal}
\makedefinition{Orthonormal}{dfn:prerequisites:orthonormal}{
   Two vectors \( \Bx, \By \) are orthonormal if they are both unit vectors and normal to each other (\( \Bx \cdot \By = 0 \), \( \Abs{\Bx \cdot \Bx} = \Abs{\By \cdot \By} = 1 \)).

   A set of vectors \( \setlr{ \Bx, \By, \cdots, \Bz } \) is an orthonormal set if all pairs of vectors in that set are orthonormal.
}

\index{standard basis}
\makedefinition{Standard basis.}{dfn:prerequisites:standardbasis}{
   A basis
\( \setlr{ \Be_1, \Be_2, \cdots, \Be_N} \) is called a standard basis if that set is orthonormal.
} % definition

\index{Euclidean space}
\makedefinition{Euclidean space.}{dfn:prerequisites:euclideanspace}{
   A vector space with basis
   \( \setlr{ \Be_1, \Be_2, \cdots, \Be_N} \) is called \textit{Euclidean} if all the dot product pairs between the basis elements are not only orthonormal, but positive definite.
That is
\begin{equation*}
\Be_i \cdot \Be_j = \delta_{ij}.
\end{equation*}
} % definition
